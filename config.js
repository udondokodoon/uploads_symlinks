(function() {
  module.exports = {
    IMG_ROOT: "/var/repos/kamihime-uploads",
    UPLOAD_HOME: "/data/sftpuser",
    UPLOAD_IMAGES: "/data/sftpuser/upload-images",
    UPLOAD_SCENARIOS: "/data/sftpuser/scenarios",
    SCENARIO_SUBDIRS: [
      "main_story",
      "free_story",
      "harem/job",
      "harem/summon",
      "harem/character",
    ]
  };
})((this || 0).self || global);
