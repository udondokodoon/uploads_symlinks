var gulp = require("gulp");
var watch = require("gulp-watch");

var config = require("./config.js");

var imageSymUglify = require("./lib/gulp/plugins/image-symlink-uglify.js");
var scenarioSymUglify = require("./lib/gulp/plugins/scenario-symlink-uglify.js");

var BASE_DIR = __dirname;

gulp.task("image-symlink-uglify", function() {
  return watch([
      config.UPLOAD_IMAGES + "/**/*.png",
      config.UPLOAD_IMAGES + "/**/*.jpg"
    ], { read: false })
    .pipe(imageSymUglify());
});

gulp.task("scenario-symlink-uglify", function() {
  return watch(config.SCENARIO_SUBDIRS.map(function(d) {
      return [config.UPLOAD_SCENARIOS, d, "**/*"].join("/");
    }), { read: false })
    .pipe(scenarioSymUglify());
});


gulp.task("watch", ["image-symlink-uglify", "scenario-symlink-uglify"]);
gulp.task("default", ["watch"]);

