
module.exports = function(outputFileName) {

  var gutil = require("gulp-util");
  var through = require("through2");
  var Path = require("path");
  var fs = require("fs");
  var uglify = require("../../filename-uglify.js").uglify;
  var config = require("../../../config.js");

  return through.obj(function(file, enc, cb) {
    // images
    var path = file.path;
    var ext = path.substr(path.length - 3);
    var basename = Path.basename(path, ext);
    var uglified = uglify(basename) + "." + ext;

    var cwd = process.cwd();
    process.chdir(config.IMG_ROOT);
    if (fs.existsSync(uglified)) {
      console.error("symlink exists: " + fs.readlinkSync(uglified) + " => " + uglified);
      return cb();
    }
    console.log("symlink to: " + path + " => " + uglified); 
    fs.symlinkSync(path, uglified, "file", cb);
    process.chdir(cwd);
    return cb();
  });
};
