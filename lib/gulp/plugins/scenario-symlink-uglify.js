
module.exports = function(outputFileName) {

  var gutil = require("gulp-util");
  var through = require("through2");
  var Path = require("path");
  var fs = require("fs");
  var uglify = require("../../filename-uglify.js").uglify;
  var config = require("../../../config.js");

  return through.obj(function(file, enc, cb) {
    // scenario
    var i = config.UPLOAD_SCENARIOS.length;
    var path = "";
    config.SCENARIO_SUBDIRS.forEach(function(d) {
      var n = file.path.indexOf(d, i);
      if (n < 0) {
        return;
      }
      path = file.path.substr(0, file.path.indexOf("/", n + d.length + 1)); 
    });
    if (path === "") {
      console.log("path not found!!!!!");
      return cb();
    }
    var basename = Path.basename(path);
    var parentdir = Path.basename(Path.dirname(path));

    var uglified = uglify(parentdir + "_" + basename);

    var cwd = process.cwd();
    process.chdir(config.IMG_ROOT);
    if (fs.existsSync(uglified)) {
      console.error("symlink exists: " + fs.readlinkSync(uglified) + " => " + uglified);
      return cb();
    }
    console.log("symlink to: " + path + " => " + uglified); 
    fs.symlinkSync(path, uglified, "file");
    process.chdir(cwd);
    return cb();
  });
};
