(function(global) {
  "use strict";
  
  var crypt = require("../vendor/blowfish.js").blowfish;

  var key = "bLoWfIsH";
  var outputTypeHex = 1;
  var cipherModeECB = 0;

  var encrypt = function(str) {
    return crypt.encrypt(str, key, { outputType: outputTypeHex, cipherMode: cipherModeECB });
  };

  var decrypt = function(str) {
    return crypt.decrypt(str, key, { outputType: outputTypeHex, cipherMode: cipherModeECB });
  };

  module.exports = {
    uglify: encrypt,
    decrypt: decrypt
  };

})((this || 0).self || global);
