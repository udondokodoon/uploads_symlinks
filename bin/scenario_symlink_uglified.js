(function(global) {
  "use strict";
  var Path = require("path");
  var fs = require("fs");

  var _ = require("lodash");
  var Promise =  require("bluebird");

  var cwd = process.cwd();
  process.on("exit", function() {
    process.chdir(cwd);
  });

  var config = require("../config.js");
  var uglify = require("../lib/filename-uglify.js").uglify;
  var symlinkAsync = Promise.promisify(fs.symlink);

  _.each(config.SCENARIO_SUBDIRS, function(target) {
    process.chdir(config.IMG_ROOT);
    var dir = config.UPLOAD_SCENARIOS + "/" + target;
    var i = 0;
    var j = 0;
    if (!fs.existsSync(dir)) {
      console.warn("no such file or directory: " + dir);
      return;
    }
    if (!fs.statSync(dir).isDirectory()) {
      console.warn("no such directory: " + dir);
      return;
    }

    fs.readdir(dir, function(err, files) {
      if (err) {
        console.error("readdir failed: " + err);
        process.exit();
      }
      Promise.all(_.map(files, function(path) {
        path = dir + "/" + path;
        var basename = Path.basename(path);
        var parentdir = Path.basename(Path.dirname(path));
        var uglified = uglify(parentdir + "_" + basename);
        if (fs.existsSync(uglified)) {
          console.error("symlink exists: " + fs.readlinkSync(uglified) + " => " + uglified);
          j++;
          return;
        }

        console.log("symlink to: " + path + " => " + uglified);
        i++;
        return symlinkAsync(path, uglified, "dir");

      })).then(function() {
        console.log("all symlinks created for: " + target + ": " + i + "(created), " + j + "(exists) / " + files.length);
      }, function(e) {
        console.error("failed to create symlinks for: " + target + ": " + e);
      });
    });
  });

})((this || 0).self || global);
