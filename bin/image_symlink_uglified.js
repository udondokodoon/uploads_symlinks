(function(global) {
  "use strict";
  var Path = require("path");
  var fs = require("fs");

  var _ = require("lodash");
  var Promise =  require("bluebird");

  var cwd = process.cwd();
  process.on("exit", function() {
    process.chdir(cwd);
  }); 

  var config = require("../config.js");
  var uglify = require("../lib/filename-uglify.js").uglify;
  var symlinkAsync = Promise.promisify(fs.symlink);
 
  var recursive = require("recursive-readdir"); 

  recursive(config.UPLOAD_IMAGES, /*["*.png", "*.jpg"],*/ function(err, files) {
    if (err) {
      console.error("readdir failed: " + err);
      process.exit();
    }

    process.chdir(config.IMG_ROOT);
    var i = 0; 

    Promise.all(_.map(files, function(path) {
      var ext = path.substr(path.length - 3);
      var basename = Path.basename(path, ext);
      var uglified = uglify(basename) + "." + ext;
      if (fs.existsSync(uglified)) {
        console.error("symlink exists: " + fs.readlinkSync(uglified) + " => " + uglified);
        return;
      }

      console.log("symlink to: " + path + " => " + uglified); 
      i++;
      return symlinkAsync(path, uglified, "file");

    })).then(function() {
      console.log("all symlinks created for: upload-images:", i + "(created) / " + files.length);
    }, function(e) {
      console.error("failed to create symlinks for: upload-images");
    }).finally(function() {
      process.cwd(cwd);
    }); 
  });
  
})((this || 0).self || global);
