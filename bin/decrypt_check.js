
(function(global) {
  "use strict";
  var Path = require("path");
  var fs = require("fs");

  var _ = require("lodash");
  var Promise =  require("bluebird");

  var config = require("../config.js");
  var crypt = require("../vendor/blowfish.js").blowfish;

  var cwd = process.cwd();
  process.on("exit", function() {
    process.chdir(cwd);
  }); 

  var decrypt = require("../lib/filename-uglify.js").decrypt;
  _.each(fs.readdirSync(config.IMG_ROOT), function(file) {
    console.log(file + " => "+ decrypt(file));
  });

})((this || 0).self || global);
